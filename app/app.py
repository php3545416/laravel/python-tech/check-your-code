import sys
from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route('/', methods=['POST'])
def home():
    code_area_data = request.get_json()['code_area']
    input = request.get_json()['input']

    try:
        original_stdout = sys.stdout
        sys.stdout = open('output.txt', 'w')

        exec(code_area_data, {})

        sys.stdout.close()

        sys.stdout = original_stdout

        output = open('output.txt', 'r').read()
    except Exception as e:
        sys.stdout = original_stdout
        output = str(e)

    return jsonify({"code":code_area_data , "output":output})

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=5000)